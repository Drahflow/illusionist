#ifndef H_F7524126_2CD1_44C9_B954_4E9D602FF8DF
#define H_F7524126_2CD1_44C9_B954_4E9D602FF8DF

#include <SDL2/SDL.h>
#include <cairo/cairo.h>

static constexpr int width = 1024;
static constexpr int height = 768;

#endif
