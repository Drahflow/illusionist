CXX=g++-10
CXXFLAGS=-ggdb -W -Wall -Wextra -Werror -pedantic --std=c++20 -fconcepts -D_GNU_SOURCE
DEPFLAGS=-MD -MP
OPTIMIZEFLAGS=-O4
LIBRARIES=-lSDL2 -lcairo

SOURCES := $(wildcard *.c++) $(wildcard scene/*.c++ serialize/*.c++ semantics/*.c++)
OBJECTS := $(SOURCES:%.c++=%.o)
DEPENDENCIES := $(OBJECTS:.o=.d)
DEPENDENCIES := $(patsubst %,build/%,$(DEPENDENCIES))
OBJECTS := $(patsubst %,build/%,$(OBJECTS))

all: illusionist

illusionist: $(OBJECTS)
	$(CXX) $(OPTIMIZEFLAGS) $(CXXFLAGS) -o $@ $^ $(LIBRARIES)

build/%.o: %.c++
	$(CXX) $(OPTIMIZEFLAGS) $(CXXFLAGS) $(DEPFLAGS) -c -o $@ $<

clean:
	rm -rfv build illusionist

.PHONY: all clean

.PRECIOUS: build/*.o build/*.d coverage/*.o

-include $(DEPENDENCIES)
