#ifndef H_912A949C_8CF4_43BF_A9DB_0BD57AB0B1F8
#define H_912A949C_8CF4_43BF_A9DB_0BD57AB0B1F8

#include <iosfwd>

namespace scene {
  class ErrorDisplay {
    public:
      virtual std::ostream &error() = 0;
  };
}

#endif
