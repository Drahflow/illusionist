#ifndef H_73C6A4BA_DE05_4CD3_8683_4219F76D163D
#define H_73C6A4BA_DE05_4CD3_8683_4219F76D163D

#include "plane.h"
#include "plane_mixins.h"
#include "screen_point.h"
#include "plane_region.h"

#include <vector>
#include <memory>

namespace scene {
  class PlaneContent;
  class PlaneConnector;

  class UniformPlane: public serialize::Serializable<UniformPlane>, public Plane, private PlaneMixins {
    SERIALIZABLE("scene::UniformPlane")

    public:
      UniformPlane();

      void draw(cairo_t *const c) override;
      bool handleEvent(const SDL_Event &e) override;

    private:
      float r, g, b;

      int focusU, focusV;
      ScreenPoint scroll;
      float zoom, zoomTarget;
      PlaneContent *keyboardFocus;

      PlaneRegion selectedRegion;
      std::vector<SDL_KeyCode> commandSequence;

      void handleSelection(bool clear);

      std::vector<std::unique_ptr<PlaneContent>> contents;

      bool isEmpty(int u, int v) const;
      bool isEmpty(const PlaneRegion &) const;
      bool isEmptyOrZoned(int u, int v) const;
      bool isEmptyOrZoned(const PlaneRegion &) const;
      bool canZone(const PlaneRegion &) const;
      void insert(std::unique_ptr<PlaneContent> &&p);
      void insertConnection(int du, int dv);
      void remove(const PlaneContent *const);
      void remove(int u, int v);
      void remove(const PlaneRegion &);
      PlaneContent *find(int u, int v);
      PlaneConnector *findConnector(int u, int v);
      PlaneContent *findSmallest(int u, int v);

      PlaneRegion appliedRegion() const;

    public:
      void serialize(serialize::Storage &) override;
  };
}

#endif
