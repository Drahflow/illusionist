#ifndef H_034D466E_C935_4C2C_96DB_FF7087FB99B6
#define H_034D466E_C935_4C2C_96DB_FF7087FB99B6

#include "plane_region.h"
#include "../common.h"

#include "../serialize/serializable.h"

#include <functional>
#include <vector>

namespace scene {
  class PlaneContent: public serialize::Serializable<PlaneContent> {
    SERIALIZABLE("scene::PlaneContent")

    public:
      virtual ~PlaneContent() { }

      struct Draw {
        enum {
          BACK,
          FRONT
        } phase;
        PlaneRegion region;
        std::function<void(cairo_t *)> draw;
      };

      virtual bool intersects(const PlaneRegion &) const = 0;
      virtual bool intersectsBorder(const PlaneRegion &) const = 0;
      virtual bool within(const PlaneRegion &) const = 0;
      virtual unsigned int size() const = 0;

      virtual void enqueueDraw(std::vector<Draw> &) const = 0;

      virtual bool wantsKeyboard(const SDL_Event &) { return false; }
      virtual bool handleEvent(const SDL_Event &) { return false; }
  };
}

#endif
