#ifndef H_AACFA14F_6D97_40C6_88D7_066D81E8C7B4
#define H_AACFA14F_6D97_40C6_88D7_066D81E8C7B4

#include "plane_content.h"
#include "plane_mixins.h"

#include "../serialize/serializable.h"

namespace scene {
  class PlaneSolder: public serialize::Serializable<PlaneSolder>, public PlaneContent, private PlaneMixins {
    SERIALIZABLE("scene::PlaneSolder")

    public:
      PlaneSolder() { }
      PlaneSolder(int u, int v);

      bool intersects(const PlaneRegion &) const override;
      bool intersectsBorder(const PlaneRegion &) const override;
      bool within(const PlaneRegion &) const override;
      unsigned int size() const override;

      void enqueueDraw(std::vector<Draw> &) const override;

      void addEdge(int du, int dv);
      void removeEdge(int du, int dv);

      void serialize(serialize::Storage &) override;

    protected:
      float height() const;

    private:
      int _u, _v;

      // Bitfield
      int edges;
      static int edge(int du, int dv);


      void drawLines(cairo_t *) const;
  };
}

#endif
