#include "plane_solder.h"

#include "../serialize/storage.h"
#include "screen_state_savepoint.h"

using namespace scene;

PlaneSolder::PlaneSolder(int u, int v)
  : _u(u), _v(v), edges(0) { }

void PlaneSolder::addEdge(int du, int dv) {
  edges |= edge(du, dv);
}

void PlaneSolder::removeEdge(int du, int dv) {
  edges &= ~edge(du, dv);
}

bool PlaneSolder::intersects(const PlaneRegion &region) const {
  return region.covers(_u, _v);
}

bool PlaneSolder::within(const PlaneRegion &region) const {
  return intersects(region);
}

bool PlaneSolder::intersectsBorder(const PlaneRegion &) const {
  return false;
}

unsigned int PlaneSolder::size() const {
  return 1;
}

void PlaneSolder::drawLines(cairo_t *c) const {
  auto self = center(_u, _v);

  for(int du = -1; du < 2; ++du) {
    for(int dv = -1; dv < 2; ++dv) {
      if(edges & edge(du, dv)) {
        auto to = (self + center(_u + du, _v + dv)) / 2;

        cairo_move_to(c, self.x, self.y);
        cairo_line_to(c, to.x, to.y);
      }
    }
  }
  cairo_stroke(c);

  auto t = self * 0.9 + center(_u + 1, _v + 1) * 0.1;
  auto l = self * 0.9 + center(_u - 1, _v + 1) * 0.1;
  auto b = self * 0.9 + center(_u - 1, _v - 1) * 0.1;
  auto r = self * 0.9 + center(_u + 1, _v - 1) * 0.1;

  cairo_move_to(c, t.x, t.y);
  cairo_line_to(c, l.x, l.y);
  cairo_line_to(c, b.x, b.y);
  cairo_line_to(c, r.x, r.y);
  cairo_fill(c);
}

void PlaneSolder::enqueueDraw(std::vector<Draw> &list) const {
  list.push_back({
    .phase = Draw::FRONT,
    .region = {_u, _v, _u + 1, _v + 1 },
    .draw = [this](cairo_t *c) {
      cairo_set_line_width(c, 1);
      cairo_set_source_rgb(c, 0.2, 0.2, 0.4);
      drawLines(c);

      ScreenStateSavepoint sssp(c);
      cairo_translate(c, 0, -height());

      cairo_set_line_width(c, 1);
      cairo_set_source_rgb(c, 1, 1, 1);
      drawLines(c);
    },
  });
}

float PlaneSolder::height() const {
  return 10.0f;
}

void PlaneSolder::serialize(serialize::Storage &s) {
  s & "u", _u;
  s & "v", _v;
  s & "edges", edges;
}

int PlaneSolder::edge(int du, int dv) {
  return 1 << (du + dv * 3 + 4);
}

REGISTER_SERIALIZATION(PlaneSolder)
