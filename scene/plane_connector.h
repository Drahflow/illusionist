#ifndef H_0E433A19_19C8_4BED_8BB4_6A190B04147A
#define H_0E433A19_19C8_4BED_8BB4_6A190B04147A

#include "plane_content.h"
#include "plane_mixins.h"

#include "../serialize/serializable.h"

namespace scene {
  class PlaneConnector: public serialize::Serializable<PlaneConnector>, public PlaneContent, private PlaneMixins {
    SERIALIZABLE("scene::PlaneConnector")

    public:
      PlaneConnector() { }
      PlaneConnector(int u, int v);

      bool intersects(const PlaneRegion &) const override;
      bool intersectsBorder(const PlaneRegion &) const override;
      bool within(const PlaneRegion &) const override;
      unsigned int size() const override;

      void enqueueDraw(std::vector<Draw> &) const override;

      void setIncoming(int du, int dv);
      void setOutgoing(int du, int dv);

      void serialize(serialize::Storage &) override;

    protected:
      float height() const;

    private:
      int _u, _v;

      // Zero if in/out is not defined
      int inDu, inDv;
      int outDu, outDv;

      void drawLines(cairo_t *) const;
  };
}

#endif
