#ifndef H_9885E2EC_5B89_4659_97D0_8ADF5D5C4861
#define H_9885E2EC_5B89_4659_97D0_8ADF5D5C4861

struct ScreenPoint {
  float x, y;

  ScreenPoint operator + (const ScreenPoint &other) const {
    return {
      x + other.x,
      y + other.y,
    };
  }

  ScreenPoint operator - (const ScreenPoint &other) const {
    return {
      x - other.x,
      y - other.y,
    };
  }

  ScreenPoint operator / (const float scalar) const {
    return {
      x / scalar,
      y / scalar,
    };
  }

  ScreenPoint operator * (const float scalar) const {
    return {
      x * scalar,
      y * scalar,
    };
  }
};

#endif
