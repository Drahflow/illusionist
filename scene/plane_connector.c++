#include "plane_connector.h"

#include "../serialize/storage.h"
#include "screen_state_savepoint.h"

using namespace scene;

PlaneConnector::PlaneConnector(int u, int v)
  : _u(u), _v(v), inDu(0), inDv(0), outDu(0), outDv(0) { }

void PlaneConnector::setIncoming(int du, int dv) {
  inDu = du;
  inDv = dv;
}

void PlaneConnector::setOutgoing(int du, int dv) {
  outDu = du;
  outDv = dv;
}

bool PlaneConnector::intersects(const PlaneRegion &region) const {
  return region.covers(_u, _v);
}

bool PlaneConnector::intersectsBorder(const PlaneRegion &) const {
  return false;
}

bool PlaneConnector::within(const PlaneRegion &region) const {
  return intersects(region);
}

unsigned int PlaneConnector::size() const {
  return 1;
}

void PlaneConnector::drawLines(cairo_t *c) const {
  auto self = center(_u, _v);
  auto from = (self + center(_u + inDu, _v + inDv)) / 2;
  auto to = (self + center(_u + outDu, _v + outDv)) / 2;

  cairo_move_to(c, from.x, from.y);
  cairo_line_to(c, self.x, self.y);
  cairo_line_to(c, to.x, to.y);

  if(outDu + outDv >= 0) {
    switch(outDu + outDv * 3) {
      case -2:
        cairo_move_to(c, to.x - 10 * 1, to.y - 5);
        cairo_line_to(c, to.x, to.y);
        cairo_line_to(c, to.x - 10 * 1, to.y + 5);
        break;
      case 1:
        cairo_move_to(c, to.x - 5 - 5 * 1.5, to.y - 2.5 + 2.5 * 1.5);
        cairo_line_to(c, to.x, to.y);
        cairo_line_to(c, to.x + 5 - 5 * 1.5, to.y + 2.5 + 2.5 * 1.5);
        break;
      case 2:
        cairo_move_to(c, to.x + 10 * 1, to.y - 5);
        cairo_line_to(c, to.x, to.y);
        cairo_line_to(c, to.x + 10 * 1, to.y + 5);
        break;
      case 3:
        cairo_move_to(c, to.x + 5 + 5 * 1.5, to.y - 2.5 + 2.5 * 1.5);
        cairo_line_to(c, to.x, to.y);
        cairo_line_to(c, to.x - 5 + 5 * 1.5, to.y + 2.5 + 2.5 * 1.5);
        break;
      case 4:
        cairo_move_to(c, to.x + 7.5, to.y + 5);
        cairo_line_to(c, to.x, to.y);
        cairo_line_to(c, to.x - 7.5, to.y + 5);
        break;
    }
  }
}

void PlaneConnector::enqueueDraw(std::vector<Draw> &list) const {
  list.push_back({
    .phase = Draw::FRONT,
    .region = {_u, _v, _u + 1, _v + 1 },
    .draw = [this](cairo_t *c) {
      drawLines(c);
      cairo_set_line_width(c, 1);
      cairo_set_source_rgb(c, 0.2, 0.2, 0.4);
      cairo_stroke(c);

      ScreenStateSavepoint sssp(c);
      cairo_translate(c, 0, -height());

      drawLines(c);
      cairo_set_line_width(c, 1);
      cairo_set_source_rgb(c, 1, 1, 1);
      cairo_stroke(c);
    },
  });
}

float PlaneConnector::height() const {
  return 10.0f;
}

void PlaneConnector::serialize(serialize::Storage &s) {
  s & "u", _u;
  s & "v", _v;
  s & "inDu", inDu;
  s & "inDv", inDv;
  s & "outDu", outDu;
  s & "outDv", outDv;
}

REGISTER_SERIALIZATION(PlaneConnector)
