#include "plane_mixins.h"

#include "../common.h"

using namespace scene;

ScreenPoint PlaneMixins::center(int u, int v) {
  return {
    width / 2 + u * 40.0f - v * 40.0f,
    height / 2 - u * 20.0f - v * 20.0f,
  };
}

ScreenPoint PlaneMixins::left(int u, int v) {
  auto c = center(u, v);
  return {
    c.x - 40,
    c.y
  };
}

ScreenPoint PlaneMixins::right(int u, int v) {
  auto c = center(u, v);
  return {
    c.x + 40,
    c.y
  };
}

ScreenPoint PlaneMixins::top(int u, int v) {
  auto c = center(u, v);
  return {
    c.x,
    c.y - 20,
  };
}

ScreenPoint PlaneMixins::bottom(int u, int v) {
  auto c = center(u, v);
  return {
    c.x,
    c.y + 20,
  };
}
