#ifndef H_40751FD3_0F9E_4E5C_B239_8AC88143E99E
#define H_40751FD3_0F9E_4E5C_B239_8AC88143E99E

#include <algorithm>

#include "../serialize/serializable.h"

namespace scene {
  struct PlaneRegion: public serialize::Serializable<PlaneRegion> {
    SERIALIZABLE("scene::PlaneRegion")

    int beginU, beginV;
    int endU, endV;

    PlaneRegion(): beginU(0), beginV(0), endU(0), endV(0) { }
    PlaneRegion(int beginU, int beginV, int endU, int endV): beginU(beginU), beginV(beginV), endU(endU), endV(endV) { }

    explicit PlaneRegion(int u, int v): PlaneRegion() {
      extendTo(u, v);
    }

    bool empty() const {
      return endU <= beginU || endV <= beginV;
    }

    bool covers(int u, int v) const {
      return beginU <= u && u < endU && beginV <= v && v < endV;
    }

    bool intersects(const PlaneRegion &other) const {
      return beginU < other.endU && other.beginU < endU &&
         beginV < other.endV && other.beginV < endV;
    }

    bool within(const PlaneRegion &other) const {
      return other.beginU <= beginU && endU <= other.endU &&
        other.beginV <= beginV && endV <= other.endV;
    }

    bool intersectsBorder(const PlaneRegion &other) const {
      bool intersectsU = beginU < other.endU && other.beginU < endU;
      bool intersectsV = beginV < other.endV && other.beginV < endV;

      if(intersectsU) {
        if(beginV == other.beginV) return true;
        if(beginV == other.endV) return true;
        if(endV == other.beginV) return true;
        if(endV == other.endV) return true;

        if(beginV < other.beginV && other.beginV < endV && endV < other.endV) return true;
        if(other.beginV < beginV && beginV < other.endV && other.endV < endV) return true;
      }

      if(intersectsV) {
        if(beginU == other.beginU) return true;
        if(beginU == other.endU) return true;
        if(endU == other.beginU) return true;
        if(endU == other.endU) return true;

        if(beginU < other.beginU && other.beginU < endU && endU < other.endU) return true;
        if(other.beginU < beginU && beginU < other.endU && other.endU < endU) return true;
      }

      return false;
    }

    void clear() {
      beginU = endU = beginV = endV = 0;
    }

    unsigned int size() const {
      int ret = (endU - beginU) * (endV - beginV);
      return ret > 0? ret: 0;
    }

    void extendTo(int u, int v) {
      using namespace std;

      if(empty()) {
        endU = (beginU = u) + 1;
        endV = (beginV = v) + 1;
      } else {
        beginU = min(u, beginU);
        beginV = min(v, beginV);
        endU = max(u + 1, endU);
        endV = max(v + 1, endV);
      }
    }

    void serialize(serialize::Storage &) override;
  };
}

#endif
