#ifndef H_01FD7BC9_61F2_4099_82C5_4AEB8577E559
#define H_01FD7BC9_61F2_4099_82C5_4AEB8577E559

#include "plane_content.h"
#include "plane_mixins.h"
#include "plane_region.h"

#include "../serialize/serializable.h"

namespace scene {
  class PlaneZone: public serialize::Serializable<PlaneZone>, public PlaneContent, private PlaneMixins {
    SERIALIZABLE("scene::PlaneZone")

    public:
      PlaneZone() { }
      PlaneZone(const PlaneRegion &region);

      bool intersects(const PlaneRegion &) const override;
      bool intersectsBorder(const PlaneRegion &) const override;
      bool within(const PlaneRegion &) const override;
      unsigned int size() const override;

      void enqueueDraw(std::vector<Draw> &) const override;

    protected:
      float height() const;

    private:
      PlaneRegion region;

      void serialize(serialize::Storage &) override;
  };
}

#endif
