#include "plane_box.h"

#include "../serialize/storage.h"
#include "screen_state_savepoint.h"

using namespace scene;

PlaneBox::PlaneBox(const PlaneRegion &region)
  : region(region)
{ }

void PlaneBox::enqueueDraw(std::vector<Draw> &list) const {
  list.push_back({
    .phase = Draw::FRONT,
    .region = region,
    .draw = [this](cairo_t *c) {
      auto l = left(region.beginU, region.endV - 1);
      auto b = bottom(region.beginU, region.beginV);
      auto r = right(region.endU - 1, region.beginV);
      auto t = top(region.endU - 1, region.endV - 1);
      auto m = (l + r + t + b) / 4;

      cairo_set_line_width(c, 0.5);
      cairo_move_to(c, l.x, l.y);
      cairo_line_to(c, b.x, b.y);
      cairo_line_to(c, r.x, r.y);
      cairo_line_to(c, r.x, r.y - height());
      cairo_line_to(c, t.x, t.y - height());
      cairo_line_to(c, l.x, l.y - height());
      cairo_close_path(c);
      cairo_set_source_rgb(c, 0.6, 0.6, 1);
      cairo_fill(c);

      cairo_move_to(c, l.x, l.y);
      cairo_line_to(c, b.x, b.y);
      cairo_line_to(c, r.x, r.y);
      cairo_line_to(c, r.x, r.y - height());
      cairo_line_to(c, t.x, t.y - height());
      cairo_line_to(c, l.x, l.y - height());
      cairo_close_path(c);
      cairo_move_to(c, l.x, l.y - height());
      cairo_line_to(c, b.x, b.y - height());
      cairo_line_to(c, r.x, r.y - height());
      cairo_move_to(c, b.x, b.y - height());
      cairo_line_to(c, b.x, b.y);

      cairo_set_source_rgb(c, 0.2, 0.2, 0.2);
      cairo_stroke(c);

      {
        ScreenStateSavepoint sssp(c);

        const std::string lbl = labelEditing? *labelEditing: label;
        cairo_text_extents_t extends;
        cairo_text_extents(c, lbl.c_str(), &extends);

        cairo_set_source_rgb(c, 0.4, 0.4, 0.6);
        auto du = (center(1, 0) - center(0, 0)) / 20.0;
        auto dv = (center(0, 1) - center(0, 0)) / -20.0;

        if(region.endU - region.beginU >= region.endV - region.beginV) {
          cairo_matrix_t textMatrix = {
            .xx = du.x,
            .yx = du.y,
            .xy = dv.x,
            .yy = dv.y,
            .x0 = m.x           ,
            .y0 = m.y - height(),
          };
          cairo_transform(c, &textMatrix);
        } else {
          cairo_matrix_t textMatrix = {
            .xx = dv.x,
            .yx = dv.y,
            .xy = -du.x,
            .yy = -du.y,
            .x0 = m.x           ,
            .y0 = m.y - height(),
          };
          cairo_transform(c, &textMatrix);
        }

        cairo_move_to(c, -extends.width / 2, -extends.height / 2 - extends.y_bearing);
        cairo_show_text(c, lbl.c_str());
      }
    }
  });
}

bool PlaneBox::intersects(const PlaneRegion &r) const {
  return region.intersects(r);
}

bool PlaneBox::intersectsBorder(const PlaneRegion &r) const {
  return region.intersectsBorder(r);
}

bool PlaneBox::within(const PlaneRegion &r) const {
  return region.within(r);
}

unsigned int PlaneBox::size() const {
  return region.size();
}

float PlaneBox::height() const {
  return 25.0f;
}

bool PlaneBox::wantsKeyboard(const SDL_Event &) {
  labelEditing = label;

  SDL_StartTextInput();
  return true;
}

bool PlaneBox::handleEvent(const SDL_Event &e) {
  switch(e.type) {
    case SDL_TEXTINPUT:
      *labelEditing += e.text.text;
      return true;

    case SDL_KEYDOWN:
      {
        if(e.key.keysym.sym == SDLK_ESCAPE) {
          SDL_StopTextInput();
          return false;
        } else if(e.key.keysym.sym == SDLK_KP_ENTER ||
            e.key.keysym.sym == SDLK_RETURN) {
          label = *labelEditing;
          SDL_StopTextInput();
          return false;
        } else if(e.key.keysym.sym == SDLK_BACKSPACE) {
          if(!labelEditing->empty()) {
            labelEditing->pop_back();
          }
          return true;
        }
      }
      return true;

    default:
      return false;
  }
}

void PlaneBox::serialize(serialize::Storage &s) {
  s & "region", region;
  s & "label", label;
}

REGISTER_SERIALIZATION(PlaneBox)
