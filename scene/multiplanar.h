#ifndef H_06257C51_99A8_4802_8E0C_87CF188E2BDF
#define H_06257C51_99A8_4802_8E0C_87CF188E2BDF

#include "../common.h"
#include "composition.h"
#include "error_display.h"

#include "../serialize/serializable.h"

namespace scene {
  class Plane;

  class Multiplanar: public serialize::Serializable<Multiplanar>, public Composition<Plane>, public ErrorDisplay {
    SERIALIZABLE("scene::Multiplanar")

    public:

      void draw(cairo_t *const c) override;
      bool handleEvent(const SDL_Event &e) override;
      std::ostream &error() override;

      void serialize(serialize::Storage &) override;
  };
}

#endif
