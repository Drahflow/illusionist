#ifndef H_9D54749D_0E95_43DB_971B_089A911BFFCE
#define H_9D54749D_0E95_43DB_971B_089A911BFFCE

#include "../common.h"

namespace scene {
  class ScreenStateSavepoint {
    private:
      cairo_t *c;
      cairo_matrix_t matrix;

    public:
      ScreenStateSavepoint(cairo_t *);
      ~ScreenStateSavepoint();
  };
}

#endif
