#ifndef H_D4EC4729_6CD5_4373_9FC4_A647DF203BBD
#define H_D4EC4729_6CD5_4373_9FC4_A647DF203BBD

#include "screen_point.h"

namespace scene {
  class PlaneMixins {
    protected:
      static ScreenPoint center(int u, int v);
      static ScreenPoint left(int u, int v);
      static ScreenPoint bottom(int u, int v);
      static ScreenPoint right(int u, int v);
      static ScreenPoint top(int u, int v);
  };
}

#endif
