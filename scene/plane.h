#ifndef H_1239F7AF_7D95_4226_B092_7CDDD670D040
#define H_1239F7AF_7D95_4226_B092_7CDDD670D040

#include "../common.h"

#include "../serialize/serializable.h"

namespace scene {
  class Plane: public serialize::Serializable<Plane> {
    SERIALIZABLE("scene::Plane")

    public:
      virtual ~Plane() { }
      virtual void draw(cairo_t *const c) = 0;
      virtual bool handleEvent(const SDL_Event &e) = 0;
  };
}

#endif
