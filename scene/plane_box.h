#ifndef H_A641691E_71FE_433F_BE2C_90D07D3E29DD
#define H_A641691E_71FE_433F_BE2C_90D07D3E29DD

#include "plane_content.h"
#include "plane_mixins.h"
#include "plane_region.h"

#include "../serialize/serializable.h"

namespace scene {
  class PlaneBox: public serialize::Serializable<PlaneBox>, public PlaneContent, private PlaneMixins {
    SERIALIZABLE("scene::PlaneBox")

    public:
      PlaneBox() { }
      PlaneBox(const PlaneRegion &region);

      bool intersects(const PlaneRegion &) const override;
      bool intersectsBorder(const PlaneRegion &) const override;
      bool within(const PlaneRegion &) const override;
      unsigned int size() const override;

      void enqueueDraw(std::vector<Draw> &) const override;
      void serialize(serialize::Storage &) override;

      bool wantsKeyboard(const SDL_Event &) override;
      bool handleEvent(const SDL_Event &) override;

    protected:
      float height() const;

    private:
      PlaneRegion region;
      std::string label;
      std::optional<std::string> labelEditing;

    public: // Interface to semantics::
      const std::string &getLabel();
  };
}

#endif
