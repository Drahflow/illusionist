#include "plane_region.h"

#include "../serialize/storage.h"

using namespace scene;

void PlaneRegion::serialize(serialize::Storage &s) {
  s & "beginU", beginU;
  s & "beginV", beginV;
  s & "endU", endU;
  s & "endV", endV;
}

REGISTER_SERIALIZATION(PlaneRegion)
