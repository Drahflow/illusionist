#include "screen_state_savepoint.h"

using namespace scene;

ScreenStateSavepoint::ScreenStateSavepoint(cairo_t *c): c(c) {
  cairo_get_matrix(c, &matrix);
}

ScreenStateSavepoint::~ScreenStateSavepoint() {
  cairo_set_matrix(c, &matrix);
}
