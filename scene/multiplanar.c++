#include "multiplanar.h"

#include "plane.h"

#include "../serialize/storage.h"

using namespace scene;

void Multiplanar::draw(cairo_t *const c) {
  for(auto &p: components) p->draw(c);
}

bool Multiplanar::handleEvent(const SDL_Event &e) {
  for(auto &p: components) {
    if(p->handleEvent(e)) return true;
  }

  return false;
}

void Multiplanar::serialize(serialize::Storage &s) {
  s & "components", components;
}

static bool _ = Multiplanar::registerMake();
