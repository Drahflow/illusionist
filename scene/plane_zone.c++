#include "plane_zone.h"

#include "../serialize/storage.h"

using namespace scene;

PlaneZone::PlaneZone(const PlaneRegion &region)
  : region(region)
{ }

void PlaneZone::enqueueDraw(std::vector<Draw> &list) const {
  list.push_back({
    .phase = Draw::BACK,
    .region = region,
    .draw = [this](cairo_t *c) {
      auto l = left(region.beginU, region.endV - 1);
      auto t = top(region.endU - 1, region.endV - 1);
      auto r = right(region.endU - 1, region.beginV);

      cairo_set_line_width(c, 2);
      cairo_set_source_rgb(c, 0.6, 0.6, 1);

      for(float i = 0; i < r.x - t.x; i += 10) {
        cairo_move_to(c, t.x + i, t.y + i / 2 - height());
        cairo_line_to(c, t.x + i, t.y + i / 2);
      }
      for(float i = 0; i > l.x - t.x; i -= 10) {
        cairo_move_to(c, t.x + i, t.y - i / 2 - height());
        cairo_line_to(c, t.x + i, t.y - i / 2);
      }
      cairo_move_to(c, l.x, l.y - height());
      cairo_line_to(c, t.x, t.y - height());
      cairo_line_to(c, r.x, r.y - height());
      cairo_stroke(c);
    }
  });

  list.push_back({
    .phase = Draw::FRONT,
    .region = region,
    .draw = [this](cairo_t *c) {
      auto l = left(region.beginU, region.endV - 1);
      auto b = bottom(region.beginU, region.beginV);
      auto r = right(region.endU - 1, region.beginV);

      cairo_set_line_width(c, 2);
      cairo_set_source_rgb(c, 0.6, 0.6, 1);

      for(float i = 0; i <= r.x - b.x; i += 10) {
        cairo_move_to(c, b.x + i, b.y - i / 2 - height());
        cairo_line_to(c, b.x + i, b.y - i / 2);
      }
      for(float i = 0; i >= l.x - b.x; i -= 10) {
        cairo_move_to(c, b.x + i, b.y + i / 2 - height());
        cairo_line_to(c, b.x + i, b.y + i / 2);
      }
      cairo_move_to(c, l.x, l.y - height());
      cairo_line_to(c, b.x, b.y - height());
      cairo_line_to(c, r.x, r.y - height());
      cairo_stroke(c);
    }
  });
}

bool PlaneZone::intersects(const PlaneRegion &r) const {
  return region.intersects(r);
}

bool PlaneZone::intersectsBorder(const PlaneRegion &r) const {
  return region.intersectsBorder(r);
}

bool PlaneZone::within(const PlaneRegion &r) const {
  return region.within(r);
}

unsigned int PlaneZone::size() const {
  return region.size();
}

float PlaneZone::height() const {
  return 20.0f;
}

void PlaneZone::serialize(serialize::Storage &s) {
  s & "region", region;
}

REGISTER_SERIALIZATION(PlaneZone)
