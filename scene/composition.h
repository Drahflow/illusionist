#ifndef H_F8EE3ED9_5F56_4684_8E4C_DD7F37195AC9
#define H_F8EE3ED9_5F56_4684_8E4C_DD7F37195AC9

#include "../common.h"

#include <vector>
#include <memory>

namespace scene {
  template<class T> class Composition {
    protected:
      std::vector<std::unique_ptr<T>> components;

    public:
      virtual void draw(cairo_t *const c) = 0;
      virtual bool handleEvent(const SDL_Event &e) = 0;

      virtual void push_back(std::unique_ptr<T> &&t) {
        components.push_back(move(t));
      }
  };
}

#endif
