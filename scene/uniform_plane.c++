#include "uniform_plane.h"

#include "plane_content.h"
#include "plane_box.h"
#include "plane_zone.h"
#include "plane_connector.h"
#include "plane_solder.h"
#include "screen_state_savepoint.h"

#include "../serialize/storage.h"

#include <iostream>

using namespace scene;
using namespace std;

UniformPlane::UniformPlane(): r(0.4), g(0.4), b(0.8), focusU(0), focusV(0), scroll{0, 0}, zoom(1.0), zoomTarget(1.0), keyboardFocus(nullptr) {
}

template<class I, class F>
static void bubble_sort(const I &begin, const I &end, const F &less) {
  for(auto i = begin; i != end; ++i) {
    for(auto j = i + 1; j != end; ++j) {
      if(less(*j, *i)) {
        auto tmp = *j;
        *j = *i;
        *i = tmp;
      }
    }
  }
}

void UniformPlane::draw(cairo_t *const c) {
  auto focusTop = top(focusU, focusV);
  auto focusLeft = left(focusU, focusV);
  auto focusBottom = bottom(focusU, focusV);
  auto focusRight = right(focusU, focusV);
  auto focusCenter = (focusTop + focusBottom + focusLeft + focusRight) / 4;

  scroll = scroll * 0.9 + (focusCenter - ScreenPoint{width / 2, height / 2}) * 0.1;
  zoom = zoom * 0.9 + zoomTarget * 0.1;

  ScreenStateSavepoint sssp(c);

  cairo_set_source_rgb(c, r, g, b);
  cairo_rectangle(c, 0, 0, width, height);
  cairo_fill(c);

  cairo_scale(c, zoom, zoom);
  cairo_translate(c, width / 2 / zoom - width / 2, height / 2 / zoom - height / 2);
  cairo_translate(c, -scroll.x, -scroll.y);

  auto selectionTop = top(selectedRegion.endU - 1, selectedRegion.endV - 1);
  auto selectionLeft = left(selectedRegion.beginU, selectedRegion.endV - 1);
  auto selectionBottom = bottom(selectedRegion.beginU, selectedRegion.beginV);
  auto selectionRight = right(selectedRegion.endU - 1, selectedRegion.beginV);

  cairo_set_source_rgb(c, r * 1.3, g * 1.3, b * 1.1);
  cairo_move_to(c, selectionTop.x, selectionTop.y);
  cairo_line_to(c, selectionLeft.x, selectionLeft.y);
  cairo_line_to(c, selectionBottom.x, selectionBottom.y);
  cairo_line_to(c, selectionRight.x, selectionRight.y);
  cairo_fill(c);

  if(commandSequence.empty()) {
    cairo_set_source_rgb(c, r * 1.1, g * 1.1, b * 1.1);
  } else {
    cairo_set_source_rgb(c, r * 1.3, g * 1.3, b * 1.3);
  }
  cairo_move_to(c, focusTop.x, focusTop.y);
  cairo_line_to(c, focusLeft.x, focusLeft.y);
  cairo_line_to(c, focusBottom.x, focusBottom.y);
  cairo_line_to(c, focusRight.x, focusRight.y);
  cairo_fill(c);

  cairo_set_line_width(c, 0.5);
  cairo_set_source_rgb(c, r * 0.5, g * 0.5, b * 0.5);

  const int grid = 20 / zoom;

  for(auto u = focusU - grid; u < focusU + grid; ++u) {
    auto start = (center(u, focusV - grid) + center(u + 1, focusV - grid)) / 2;
    auto end = (center(u, focusV + grid) + center(u + 1, focusV + grid)) / 2;
    cairo_move_to(c, start.x, start.y);
    cairo_line_to(c, end.x, end.y);
    cairo_stroke(c);
  }
  for(auto v = focusV - grid; v < focusV + grid; ++v) {
    auto start = (center(focusU - grid, v) + center(focusU - grid, v + 1)) / 2;
    auto end = (center(focusU + grid, v) + center(focusU + grid, v + 1)) / 2;
    cairo_move_to(c, start.x, start.y);
    cairo_line_to(c, end.x, end.y);
    cairo_stroke(c);
  }

  vector<PlaneContent::Draw> draws;
  for(auto &c: contents) c->enqueueDraw(draws);

  bubble_sort(draws.begin(), draws.end(), [](auto &a, auto &b) {
    constexpr auto BACK = PlaneContent::Draw::BACK;

    auto aU = a.phase == BACK? a.region.endU - 1: a.region.beginU;
    auto aV = a.phase == BACK? a.region.endV - 1: a.region.beginV;
    auto bU = b.phase == BACK? b.region.endU - 1: b.region.beginU;
    auto bV = b.phase == BACK? b.region.endV - 1: b.region.beginV;

    bool intersectU = a.region.beginU < b.region.endU && b.region.beginU < a.region.endU;
    bool intersectV = a.region.beginV < b.region.endV && b.region.beginV < a.region.endV;

    bool aLater = (aU < bU && intersectV) || (aV < bV && intersectU) || (aU < bU && aV < bV);
    bool bLater = (bU < aU && intersectV) || (bV < aV && intersectU) || (bU < aU && bV < aV);

    return bLater && !aLater;
  });

  for(auto &d: draws) d.draw(c);
}

bool UniformPlane::handleEvent(const SDL_Event &e) {
  bool justLostKeyboardFocus = false;

  switch(e.type) {
    case SDL_TEXTEDITING:
    case SDL_TEXTINPUT:
      {
        if(keyboardFocus) {
          if(keyboardFocus->handleEvent(e)) return true;
          keyboardFocus = nullptr;
        }
      }
      return false;

    case SDL_KEYDOWN:
      {
        if(keyboardFocus) {
          if(keyboardFocus->handleEvent(e)) return true;
          keyboardFocus = nullptr;
          justLostKeyboardFocus = true;
        }

        if(commandSequence.size() == 1 && commandSequence[0] == SDLK_SPACE) {
          switch(e.key.keysym.sym) {
            case SDLK_u:
              commandSequence.clear();

              if(isEmptyOrZoned(appliedRegion())) {
                insert(make_unique<PlaneBox>(appliedRegion()));
                return true;
              }
              break;

            case SDLK_o: {
              if(selectedRegion.empty()) {
                remove(focusU, focusV);
              } else {
                remove(selectedRegion);
              }
              commandSequence.clear();
              return true;
            }

            case SDLK_PERIOD: {
              if(auto connector = findConnector(focusU, focusV); connector) {
                remove(connector);
              }

              auto solder = make_unique<PlaneSolder>(focusU, focusV);

              for(int du = -1; du < 2; ++du) {
                for(int dv = -1; dv < 2; ++dv) {
                  if(auto c = findConnector(focusU + du, focusV + dv); c) {
                    solder->addEdge(du, dv);
                  }
                }
              }

              insert(move(solder));
              return true;
            }

            case SDLK_e:
              commandSequence.push_back(SDLK_e);
              return true;

            case SDLK_a:
              commandSequence.clear();

              if(canZone(appliedRegion())) {
                insert(make_unique<PlaneZone>(appliedRegion()));
                return true;
              }
              break;

            default:
              commandSequence.clear();
              break;
          }
          return true;
        }

        if(commandSequence.size() == 2 && commandSequence[0] == SDLK_SPACE && commandSequence[1] == SDLK_e) {
          switch(e.key.keysym.sym) {
            case SDLK_KP_1: insertConnection(-1, 0); return true;
            case SDLK_KP_2: insertConnection(-1, -1); return true;
            case SDLK_KP_3: insertConnection(0, -1); return true;
            case SDLK_KP_4: insertConnection(-1, 1); return true;
            case SDLK_KP_6: insertConnection(1, -1); return true;
            case SDLK_KP_7: insertConnection(0, 1); return true;
            case SDLK_KP_8: insertConnection(1, 1); return true;
            case SDLK_KP_9: insertConnection(1, 0); return true;
            default: commandSequence.clear(); return true;
          }
        }

        handleSelection(false);
        int delta = SDL_GetModState() & KMOD_CTRL? 7: 1;

        switch(e.key.keysym.sym) {
          case SDLK_KP_1: focusU -= delta; break;
          case SDLK_KP_2: focusU -= delta; focusV -= delta; break;
          case SDLK_KP_3: focusV -= delta; break;
          case SDLK_KP_4: focusU -= delta; focusV += delta; break;
          case SDLK_KP_5: focusU = focusV = 0; break;
          case SDLK_KP_6: focusU += delta; focusV -= delta; break;
          case SDLK_KP_7: focusV += delta; break;
          case SDLK_KP_8: focusU += delta; focusV += delta; break;
          case SDLK_KP_9: focusU += delta; break;
          case SDLK_KP_PLUS: zoomTarget *= 1.5; break;
          case SDLK_KP_MINUS: zoomTarget /= 1.5; break;
          case SDLK_KP_MULTIPLY: zoomTarget = 1.0; break;

          case SDLK_SPACE: commandSequence.push_back(SDLK_SPACE); return true;
          case SDLK_RETURN:
          case SDLK_KP_ENTER: {
            if(!justLostKeyboardFocus) {
              if(auto obj = findSmallest(focusU, focusV); obj) {
                if(obj->wantsKeyboard(e)) {
                  keyboardFocus = obj;
                  return true;
                }
              }
            }
            return false;
          }

          default:
            return false;
        }

        handleSelection(true);
        return true;
      }

    default:
      return false;
  }
}

bool UniformPlane::isEmpty(int u, int v) const {
  return isEmpty(PlaneRegion(u, v));
}

bool UniformPlane::isEmpty(const PlaneRegion &region) const {
  for(auto &c: contents) {
    if(c->intersects(region)) return false;
  }

  return true;
}

bool UniformPlane::isEmptyOrZoned(int u, int v) const {
  return isEmptyOrZoned(PlaneRegion(u, v));
}

bool UniformPlane::isEmptyOrZoned(const PlaneRegion &region) const {
  for(auto &c: contents) {
    if(c->intersects(region) && !dynamic_cast<PlaneZone *>(c.get())) return false;
  }

  return true;
}

bool UniformPlane::canZone(const PlaneRegion &region) const {
  for(auto &c: contents) {
    if(c->intersectsBorder(region)) return false;
  }

  return true;
}

void UniformPlane::insert(unique_ptr<PlaneContent> &&p) {
  contents.push_back(move(p));
}

void UniformPlane::insertConnection(int du, int dv) {
  if(isEmptyOrZoned(focusU, focusV)) {
    insert(make_unique<PlaneConnector>(focusU, focusV));
  }
  if(auto con = findConnector(focusU, focusV); con) {
    con->setOutgoing(du, dv);
  }

  focusU += du;
  focusV += dv;

  if(isEmptyOrZoned(focusU, focusV)) {
    insert(make_unique<PlaneConnector>(focusU, focusV));
  }
  if(auto con = findConnector(focusU, focusV); con) {
    con->setIncoming(-du, -dv);
  }
}

PlaneContent *UniformPlane::find(int u, int v) {
  const PlaneRegion r(u, v);
  for(auto &c: contents) {
    if(c->intersects(r)) return c.get();
  }

  return nullptr;
}

PlaneConnector *UniformPlane::findConnector(int u, int v) {
  const PlaneRegion r(u, v);
  for(auto &c: contents) {
    if(c->intersects(r)) {
      if(auto con = dynamic_cast<PlaneConnector *>(c.get()); con) return con;
    }
  }

  return nullptr;
}

PlaneContent *UniformPlane::findSmallest(int u, int v) {
  PlaneContent *ret = nullptr;
  unsigned int size = ~0u;

  const PlaneRegion r(u, v);
  for(auto &c: contents) {
    if(c->intersects(r)) {
      if(unsigned int cSize = c->size(); cSize < size) {
        ret = c.get();
        size = cSize;
      }
    }
  }

  return ret;
}

void UniformPlane::remove(const PlaneContent *const content) {
  *find_if(contents.begin(), contents.end(), [&](auto &c) { return c.get() == content; }) = move(contents.back());
  contents.pop_back();
}

void UniformPlane::remove(int u, int v) {
  if(auto content = findSmallest(u, v); content) {
    remove(content);
  }
}

void UniformPlane::remove(const PlaneRegion &region) {
  for(auto i = contents.begin(); i != contents.end(); ) {
    if((*i)->within(region)) {
      *i = move(contents.back());
      contents.pop_back();
    } else {
      ++i;
    }
  }
}

void UniformPlane::handleSelection(bool clear) {
  if(SDL_GetModState() & KMOD_SHIFT) {
    selectedRegion.extendTo(focusU, focusV);
  } else if(clear) {
    selectedRegion.clear();
  }
}

PlaneRegion UniformPlane::appliedRegion() const {
  if(selectedRegion.empty()) {
    return PlaneRegion(focusU, focusV);
  }

  return selectedRegion;
}

void UniformPlane::serialize(serialize::Storage &s) {
  s & "r", r;
  s & "g", g;
  s & "b", b;
  s & "contents", contents;
}

REGISTER_SERIALIZATION(UniformPlane)
