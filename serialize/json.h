#ifndef H_E6128CF8_AE89_46B9_BA81_087606B05BC4
#define H_E6128CF8_AE89_46B9_BA81_087606B05BC4

#include "storage.h"

#include <ostream>
#include <istream>

namespace serialize {
  class SaveJson: public Storage {
    private:
      std::ostream &out;
      bool needsComma;

      void write(const std::string &);

    protected:
      bool isSavingStorage() const override { return true; }

    public:
      SaveJson(std::ostream &);
      ~SaveJson();

      void io(const std::string &, int &) override;
      void io(const std::string &, double &) override;
      void io(const std::string &, std::string &) override;
      void io(const std::string &, SerializableBase *&) override;
      void io(const std::string &, std::vector<std::unique_ptr<SerializableBase>> &) override;

      using Storage::io;
  };

  class LoadJson: public Storage {
    public:
      class ParseTree;

    private:
      std::istream &in;

      void parse();
      std::unique_ptr<ParseTree> parseTree;
      ParseTree *serializationTree;

    protected:
      bool isSavingStorage() const override { return false; }

    public:
      LoadJson(std::istream &);
      ~LoadJson();

      void io(const std::string &, int &) override;
      void io(const std::string &, double &) override;
      void io(const std::string &, std::string &) override;
      void io(const std::string &, SerializableBase *&) override;
      void io(const std::string &, std::vector<std::unique_ptr<SerializableBase>> &) override;

      using Storage::io;
  };
}

#endif
