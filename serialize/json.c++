#include "json.h"

#include "serializable.h"

#include <variant>
#include <map>
#include <cmath>

using namespace serialize;
using namespace std;

void SaveJson::write(const string &s) {
  out << '"';
  for(auto &c: s) {
    if(c == '"' || c == '\\') {
      out << '\\' << c;
    } else {
      out << c;
    }
  }
  out << '"';
}

SaveJson::SaveJson(ostream &out): out(out), needsComma(false) {
  out << '{';
}

SaveJson::~SaveJson() {
  out << '}';
}

void SaveJson::io(const string &key, int &i) {
  if(needsComma) out << ',';
  write(key);

  out << ':' << i;
  needsComma = true;
}

void SaveJson::io(const string &key, double &d) {
  if(needsComma) out << ',';
  write(key);

  out << ':' << d;
  needsComma = true;
}

void SaveJson::io(const string &key, string &s) {
  if(needsComma) out << ',';
  write(key);
  out << ':';
  write(s);
  needsComma = true;
}

void SaveJson::io(const string &key, SerializableBase *&obj) {
  if(needsComma) out << ',';
  write(key);
  out << ":{\"\":";
  write(obj->getSerializationId());
  needsComma = true;
  obj->serialize(*this);
  out << "}";
}

void SaveJson::io(const string &key, vector<unique_ptr<SerializableBase>> &vec) {
  if(needsComma) out << ',';
  write(key);
  out << ":[";
  bool needsArrayComma = false;
  for(auto &ptr: vec) {
    if(needsArrayComma) out << ',';
    needsArrayComma = true;
    out << "{\"\":";
    write(ptr->getSerializationId());
    needsComma = true;
    ptr->serialize(*this);
    out << '}';
  }
  out << "]";
}

class ParseError: public string { };

typedef variant<
  int64_t,
  double,
  string,
  map<string, LoadJson::ParseTree>,
  vector<LoadJson::ParseTree>,
  ParseError
> ParseTreeVariant;

class LoadJson::ParseTree: public ParseTreeVariant {
  /* nearly self-referential std::variant */

  public:
    using ParseTreeVariant::operator =;
};

LoadJson::LoadJson(istream &in): in(in) {
  parse();
  if(auto parseError = get_if<ParseError>(parseTree.get()); parseError) throw *parseError;

  serializationTree = parseTree.get();
}

LoadJson::~LoadJson() { }

#define DIGIT \
       '0': \
  case '1': \
  case '2': \
  case '3': \
  case '4': \
  case '5': \
  case '6': \
  case '7': \
  case '8': \
  case '9'

#define WHITE \
       ' ': \
  case '\n': \
  case '\r': \
  case '\t'


#include <iostream>
void LoadJson::parse() {
  enum {
    VALUE,
    NUMBER_INT,
    NUMBER_FRAC,
    NUMBER_EXP,
    KEY,
    KEY_STRING,
    COLON,
    VALUE_COMPLETE,
    STRING,
    STRING_ESCAPE
  } state = VALUE;

  bool numIsNegative = false;
  bool numExpIsNegative = false;
  int64_t numInt = 0;
  int64_t numExp = 0;
  string key;

  parseTree = unique_ptr<ParseTree>(new ParseTree());
  vector<ParseTree *> stack;
  // Yes, these will be pointers into vector elements,
  // but the vectors are of the parents, i.e. not changing
  // while the elements are within the stack.
  stack.push_back(parseTree.get());

  int c;
  while((c = in.get()) != remove_reference<decltype(in)>::type::traits_type::eof()) {
    // cout << "Stack depth: " << stack.size() << endl;
    // cout << "Top of stack: " << stack.back()->index() << endl;
    // cout << "State: " << state << endl;
    // cout << static_cast<char>(c) << endl;

    switch(state) {
      case VALUE:
        switch(c) {
          case WHITE: break;

          case '"':
            *stack.back() = string();
            state = STRING;
            break;

          case '-':
            numIsNegative = true;
            numExpIsNegative = false;
            *stack.back() = 0ll;
            numInt = 0;
            numExp = 0;
            state = NUMBER_INT;
            break;

          case DIGIT:
            numIsNegative = false;
            numExpIsNegative = false;
            *stack.back() = 0;
            numInt = c - '0';
            numExp = 0;
            state = NUMBER_INT;
            break;

          case '{':
            *stack.back() = map<string, ParseTree>();
            state = KEY;
            break;

          case '[':
            *stack.back() = vector<ParseTree>();
            get<vector<ParseTree>>(*stack.back()).push_back(ParseTree());
            stack.push_back(&get<vector<ParseTree>>(*stack.back()).back());
            state = VALUE;
            break;

          default:
            *parseTree = ParseError("Invalid character at VALUE");
            return;
        }
        break;

      case NUMBER_INT:
        switch(c) {
          case WHITE:
            *stack.back() = numIsNegative? -numInt: numInt;
            state = VALUE_COMPLETE;
            break;

          case ']':
          case '}':
          case ',':
            *stack.back() = numIsNegative? -numInt: numInt;
            goto nextEntry;

          case DIGIT:
            numInt *= 10;
            numInt += c - '0';
            break;

          case '.':
            state = NUMBER_FRAC;
            break;

          case 'E':
          case 'e':
            state = NUMBER_EXP;
            break;

          default:
            *parseTree = ParseError("Invalid character at NUMBER_INT");
            return;
        }
        break;

      case NUMBER_FRAC:
        switch(c) {
          case WHITE:
            *stack.back() = (numIsNegative? -numInt: numInt) * pow(10, numExpIsNegative? -numExp: numExp);
            state = VALUE_COMPLETE;
            break;

          case ']':
          case '}':
          case ',':
            *stack.back() = (numIsNegative? -numInt: numInt) * pow(10, numExpIsNegative? -numExp: numExp);
            goto nextEntry;

          case DIGIT:
            numInt *= 10;
            numInt += c - '0';
            numExp -= 1;
            break;

          case 'E':
          case 'e':
            state = NUMBER_EXP;
            break;

          default:
            *parseTree = ParseError("Invalid character at NUMBER_FRAC");
            return;
        }
        break;

      case NUMBER_EXP:
        switch(c) {
          case WHITE:
            *stack.back() = (numIsNegative? -numInt: numInt) * pow(10, numExpIsNegative? -numExp: numExp);
            state = VALUE_COMPLETE;
            break;

          case ']':
          case '}':
          case ',':
            *stack.back() = (numIsNegative? -numInt: numInt) * pow(10, numExpIsNegative? -numExp: numExp);
            goto nextEntry;

          case '-':
            numExpIsNegative = true;
            break;

          case DIGIT:
            numExp *= 10;
            numExp += c - '0';
            break;

          default:
            *parseTree = ParseError("Invalid character at NUMBER_FRAC");
            return;
        }
        break;

      case KEY:
        switch(c) {
          case WHITE: break;

          case '"':
            key = "";
            state = KEY_STRING;
            break;

          default:
            *parseTree = ParseError("Invalid character at KEY");
            return;
        }
        break;

      case KEY_STRING:
        switch(c) {
          case '"':
            stack.push_back(&get<map<string, ParseTree>>(*stack.back())[key]);
            state = COLON;
            break;
          
          case '\\':
            *parseTree = ParseError("Backslash escapes in key names not supported.");
            return;

          default:
            key += c;
            break;
        }
        break;

      case COLON:
        switch(c) {
          case WHITE: break;
          case ':':
            state = VALUE;
            break;

          default:
            *parseTree = ParseError("Invalid character at COLON");
            return;
        }
        break;

      case VALUE_COMPLETE:
        switch(c) {
          case WHITE: break;

          case ']':
          case '}':
          case ',':
            goto nextEntry;

          default:
            *parseTree = ParseError("Invalid character at VALUE_COMPLETE");
            return;
        }
        break;

      case STRING:
        switch(c) {
          case '"':
            state = VALUE_COMPLETE;
            break;
          
          case '\\':
            state = STRING_ESCAPE;
            return;

          default:
            get<string>(*stack.back()) += c;
            break;
        }
        break;

      case STRING_ESCAPE:
        switch(c) {
          case '"': get<string>(*stack.back()) += '"'; break;
          case '\\': get<string>(*stack.back()) += '\\'; break;
          case '/': get<string>(*stack.back()) += '/'; break;
          case 'b': get<string>(*stack.back()) += '\b'; break;
          case 'f': get<string>(*stack.back()) += '\f'; break;
          case 'n': get<string>(*stack.back()) += '\n'; break;
          case 'r': get<string>(*stack.back()) += '\r'; break;
          case 't': get<string>(*stack.back()) += '\t'; break;
          case 'u':
            *parseTree = ParseError("Unicode escapes not supported.");
            return;

          default:
            *parseTree = ParseError("Invalid escaped character after \\ in string");
            return;
        }
        break;
    }
    continue;

nextEntry:
    stack.pop_back();

    switch(c) {
      case ',':
        if(holds_alternative<map<string, ParseTree>>(*stack.back())) {
          state = KEY;
        } else if(auto array = get_if<vector<ParseTree>>(stack.back()); array) {
          array->push_back(ParseTree());
          stack.push_back(&array->back());
          state = VALUE;
        } else {
          *parseTree = ParseError("Unexpected comma; neither in object nor array");
          return;
        }
        break;

      case ']':
        if(holds_alternative<vector<ParseTree>>(*stack.back())) {
          state = VALUE_COMPLETE;
        } else {
          *parseTree = ParseError("Array ended with non-]");
          return;
        }
        break;

      case '}':
        if(holds_alternative<map<string, ParseTree>>(*stack.back())) {
          state = VALUE_COMPLETE;
        } else {
          *parseTree = ParseError("Object ended with non-}");
          return;
        }
        break;
    }
  }
}

void LoadJson::io(const string &key, int &i) {
  auto data = get<map<string, ParseTree>>(*serializationTree)[key];

  if(holds_alternative<int64_t>(data)) {
    i = get<int64_t>(data);
  } else if(holds_alternative<double>(data)) {
    i = get<double>(data);
  } else {
    i = 00;
  }
}

void LoadJson::io(const string &key, double &d) {
  auto data = get<map<string, ParseTree>>(*serializationTree)[key];

  if(holds_alternative<double>(data)) {
    d = get<double>(data);
  } else if(holds_alternative<int64_t>(data)) {
    d = get<int64_t>(data);
  } else {
    d = 0.0;
  }
}

void LoadJson::io(const string &key, string &s) {
  auto data = get<map<string, ParseTree>>(*serializationTree)[key];

  if(holds_alternative<string>(data)) {
    s = get<string>(data);
  } else {
    s = "";
  }
}

void LoadJson::io(const string &key, SerializableBase *&obj) {
  auto json = get<map<string, ParseTree>>(get<map<string, ParseTree>>(*serializationTree)[key]);
  auto type = get<string>(json[""]);

  auto parent = serializationTree;
  serializationTree = &get<map<string, ParseTree>>(*serializationTree)[key];
  obj = SerializableBase::make(type);
  obj->serialize(*this);
  serializationTree = parent;
}

void LoadJson::io(const string &key, vector<unique_ptr<SerializableBase>> &array) {
  auto jsonArray = get<vector<ParseTree>>(get<map<string, ParseTree>>(*serializationTree)[key]);

  auto parent = serializationTree;
  for(auto &json: jsonArray) {
    auto type = get<string>(get<map<string, ParseTree>>(json)[""]);
    serializationTree = &json;
    array.push_back(unique_ptr<SerializableBase>(SerializableBase::make(type)));
    array.back()->serialize(*this);
  }
  serializationTree = parent;
}
