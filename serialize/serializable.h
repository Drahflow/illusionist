#ifndef H_FBE2AA87_40B5_4C29_B026_E290D2215231
#define H_FBE2AA87_40B5_4C29_B026_E290D2215231

#include <string>
#include <map>
#include <functional>
#include <iostream>

namespace serialize {
  class Storage;

  class SerializableBase {
    protected:
      static std::map<std::string, std::function<SerializableBase *()>> &constructorRegistry();

    public:
      virtual const char *getSerializationId() const = 0;
      virtual void serialize(Storage &s) = 0;

      static SerializableBase *make(const std::string &type);
  };

  template<class T>
  class Serializable: public SerializableBase {
    public:
      static bool registerMake() {
        // Choose the (irrelevant) path towards SerializableBase ---v
        constructorRegistry()[T::serializationId] = []() -> Serializable<T>* { return new T(); };
        return true;
      };
  };
}

#define SERIALIZABLE(id) \
  public: \
    static constexpr const char *serializationId = id; \
    const char *getSerializationId() const override { return serializationId; }

#define REGISTER_SERIALIZATION(T) \
  static bool _ = serialize::Serializable<T>::registerMake();

#endif
