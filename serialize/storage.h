#ifndef H_8738BC4F_84D2_429C_A52A_9C489A7605ED
#define H_8738BC4F_84D2_429C_A52A_9C489A7605ED

#include <string>
#include <memory>
#include <vector>

namespace serialize {
  class SerializableBase;

  class Storage {
    private:
      struct Key {
        Storage *storage;
        const std::string &key;

        template<class T> void operator , (T &t) { storage->io(key, t); }
      };

    public:
      Key operator & (const std::string &key) { return {this, key}; }

      virtual void io(const std::string &, int &) = 0;
      virtual void io(const std::string &, double &) = 0;
      virtual void io(const std::string &, std::string &) = 0;
      virtual void io(const std::string &, SerializableBase *&) = 0;
      virtual void io(const std::string &, std::vector<std::unique_ptr<SerializableBase>> &) = 0;

      void io(const std::string &key, float &f) {
        if(isSavingStorage()) {
          double d = f;
          io(key, d);
        } else {
          double d;
          io(key, d);
          f = d;
        }
      }

    protected:
      virtual bool isSavingStorage() const = 0;

    public:
      template<class T> void io(const std::string &key, std::vector<std::unique_ptr<T>> &vec) {
        if(isSavingStorage()) {
          std::vector<std::unique_ptr<SerializableBase>> buf;
          for(auto &p: vec) buf.push_back(move(p));
          io(key, buf);
          vec.clear();
          for(auto &p: buf) vec.push_back(std::unique_ptr<T>(static_cast<T *>(p.release())));
        } else {
          std::vector<std::unique_ptr<SerializableBase>> buf;
          io(key, buf);

          vec.clear();
          for(auto &t: buf) {
            if(T *u = dynamic_cast<T *>(t.get()); u) {
              t.release();
              vec.push_back(std::unique_ptr<T>(u));
            } else {
              throw "Deserialized type did not match target static type.";
            }
          }
        }
      }

      template<class T> void io(const std::string &key, std::unique_ptr<T> &t) {
        if(isSavingStorage()) {
          SerializableBase *u = t.get();
          io(key, u);
        } else {
          SerializableBase *buf;
          io(key, buf);

          if(T *u = dynamic_cast<T *>(buf); u) {
            t.reset(u);
          } else {
            throw "Deserialized type did not match target static type.";
          }
        }
      }

      template<class T> void io(const std::string &key, T &t) {
        if(isSavingStorage()) {
          SerializableBase *u = &t;
          io(key, u);
        } else {
          SerializableBase *buf;
          io(key, buf);

          if(T *u = dynamic_cast<T *>(buf); u) {
            t = *u;
          } else {
            throw "Deserialized type did not match target static type.";
          }
        }
      }
  };
}

#endif
