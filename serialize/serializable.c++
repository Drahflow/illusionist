#include "serializable.h"

using namespace serialize;
using namespace std;

map<string, function<SerializableBase *()>> &SerializableBase::constructorRegistry() {
  static map<string, function<SerializableBase *()>> map;
  return map;
}

SerializableBase *SerializableBase::make(const std::string &type) {
  if(!constructorRegistry().contains(type)) {
    throw "Unknown type during deserialization: " + type;
  }

  return constructorRegistry().at(type)();
}
