#include "statespace.h"

namespace semantics {
  Statespace *STATESPACE_UNKNOWN = nullptr;
  Statespace *STATESPACE_NONE = STATESPACE_UNKNOWN + 1;
  State *STATE_UNKNOWN = nullptr;
}
