#ifndef H_0DC5690E_DB83_45E1_86B2_47357D5112DE
#define H_0DC5690E_DB83_45E1_86B2_47357D5112DE

#include <vector>
#include <variant>
#include <string>
#include <map>

namespace semantics {
  class Statespace;

  class State {
  };

  typedef std::string Uncomputable;

  struct Transition {
    const State *from, *to;
    std::vector<Statespace *> actions;
    std::map<Statespace *, const State *> sideEffects;
  };

  class Statespace {
    public:
      // How many connectors this has
      virtual int portCount() const = 0;

      typedef std::variant<Uncomputable, std::vector<Transition>> Barb;

      // This might be called while evaluating sideconditions, i.e. it must
      // not modify internal or external state.
      virtual Barb possibleTransitions(const State *from, std::vector<Statespace *>) = 0;

      virtual const State *currentState() const = 0;
      virtual void execute(const Transition &) = 0;
  };

  extern Statespace *STATESPACE_NONE;
  extern Statespace *STATESPACE_UNKNOWN;
  extern State *STATE_UNKNOWN;
}

#endif
