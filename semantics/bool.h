#ifndef H_48C3A5F5_9260_4ABC_8CCA_27EAEF05DEB0
#define H_48C3A5F5_9260_4ABC_8CCA_27EAEF05DEB0

#include "singleton_statespace.h"
#include "atom.h"

namespace semantics {
  class Bool: public SingletonStatespace {
    public:
      static Statespace *TRUE;
      static Statespace *FALSE;

      int portCount() const override { return 1; }

      Statespace::Barb possibleTransitions(const State *, std::vector<Statespace *>) override;
  };
}

#endif
