#ifndef H_203418FC_326D_4D78_914D_AD2A79318EEC
#define H_203418FC_326D_4D78_914D_AD2A79318EEC

#include <vector>
#include <memory>
#include <map>

namespace semantics {
  class Statespace;
  class State;

  struct Step {
    std::map<std::pair<Statespace *, int>, Statespace *> actions;
    std::map<Statespace *, const State *> states;
  };

  class Solver {
    private:
      std::vector<Statespace *> statespaces;
      std::map<std::pair<Statespace *, int>, int> connections;
      int nets = 0;

      void compactConnections();

    public:
      void add(Statespace *);
      void connect(Statespace *a, int portA, Statespace *b, int portB);

      // Takes a start point of known constraints (potentially a lot of STATESPACE_UNKNOWN)
      // and returns a list of valid next steps.
      std::vector<Step> solve(const std::map<std::pair<Statespace *, int>, Statespace *> &start);
  };
}

#endif
