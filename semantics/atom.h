#ifndef H_4AC8C119_E41A_47F8_BE09_276326B2DC3D
#define H_4AC8C119_E41A_47F8_BE09_276326B2DC3D

#include "singleton_statespace.h"

#include <string>

namespace semantics {
  class Atom: public SingletonStatespace {
    private:
      std::string name;

    public:
      int portCount() const override { return 1; }

      Statespace::Barb possibleTransitions(const State *, std::vector<Statespace *>) override;

      Atom(const std::string &name): name(name) { }
  };
}

#endif
