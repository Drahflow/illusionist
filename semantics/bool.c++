#include "bool.h"

using namespace std;
using namespace semantics;

Statespace *Bool::TRUE = new Atom("true");
Statespace *Bool::FALSE = new Atom("false");

Statespace::Barb Bool::possibleTransitions(const State *, std::vector<Statespace *>) {
  return {vector<Transition>{Transition{
    .from = this,
    .to = this,
    .actions = { TRUE },
    .sideEffects = {},
  }, Transition{
    .from = this,
    .to = this,
    .actions = { FALSE },
    .sideEffects = {},
  }}};
}
