#ifndef H_42BA349D_54ED_43C1_855B_E0C753AA1272
#define H_42BA349D_54ED_43C1_855B_E0C753AA1272

#include "statespace.h"

namespace semantics {
  class SingletonStatespace: public Statespace, public State {
    public:
      const State *currentState() const override { return this; }
      void execute(const Transition &) override { }
  };
}

#endif
