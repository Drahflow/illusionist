#include "solver.h"

#include "statespace.h"

#include <set>
#include <iostream>

using namespace std;
using namespace semantics;

void Solver::add(Statespace *s) {
  if(find(statespaces.begin(), statespaces.end(), s) != statespaces.end()) return;

  statespaces.push_back(s);
  for(int i = 0; i < s->portCount(); ++i) {
    connections[{s, i}] = ++nets;
  }
}

void Solver::connect(Statespace *a, int portA, Statespace *b, int portB) {
  int netA = connections[{a, portA}];
  int netB = connections[{b, portB}];

  for(auto &i: connections) {
    if(i.second == netB) i.second = netA;
  }
}

void Solver::compactConnections() {
  map<int, int> translation;

  for(auto &i: connections) {
    if(translation.contains(i.second)) continue;
    translation[i.second] = translation.size();
  }

  for(auto &i: connections) {
    i.second = translation[i.second];
  }

  nets = translation.size();
}

template<class M, class K, class D> const M::mapped_type &get_default(const M &map, const K &key, const D &def) {
  auto entry = map.find(key);
  if(entry == map.end()) return def;
  return entry->second;
}

vector<Step> Solver::solve(const map<pair<Statespace *, int>, Statespace *> &startConditions) {
  vector<Step> result;

  struct ExploreState {
    vector<Statespace *> actions;
    map<Statespace *, const State *> states;

    auto operator <=> (const ExploreState &other) const = default;
  };

  compactConnections();

  ExploreState start;
  start.actions = vector<Statespace *>(nets, STATESPACE_UNKNOWN);
  for(auto &i: startConditions) {
    start.actions[connections[i.first]] = i.second;
  }

  set<ExploreState> seen{start};
  vector<ExploreState> queue{start};

  while(!queue.empty()) {
    auto current = queue.back(); queue.pop_back();

    cout << "=== current state ===" << endl;
    for(size_t i = 0; i < current.actions.size(); ++i) {
      cout << "[" << i << "] = " << current.actions[i] << endl;
    }
    for(auto &[space, state]: current.states) {
      cout << "states[" << space << "] = " << state << endl;
    }

    bool incompatible = false;
    for(auto s: statespaces) {
      vector<Statespace *> input;
      for(int i = 0; i < s->portCount(); ++i) {
        input.push_back(current.actions[connections[{s, i}]]);
      }

      auto transitions = s->possibleTransitions(s->currentState(), input);
      if(holds_alternative<Uncomputable>(transitions)) continue;

      bool anyValidTransition = false;
      for(auto &t: get<vector<Transition>>(transitions)) {
        ExploreState next = current;

        if(get_default(next.states, s, STATE_UNKNOWN) != STATE_UNKNOWN && t.to != get_default(next.states, s, STATE_UNKNOWN)) {
          continue;
        }
        if(t.to != STATE_UNKNOWN) {
          next.states[s] = t.to;
        }

        bool incompatible = false;
        for(int i = 0; i < s->portCount(); ++i) {
          int varIdx = connections[{s, i}];
          if(next.actions[varIdx] != STATESPACE_UNKNOWN && t.actions[i] != next.actions[varIdx]) {
            incompatible = true;
            break;
          }
          
          if(t.actions[i] != STATESPACE_UNKNOWN) {
            next.actions[varIdx] = t.actions[i];
          }
        }
        for(auto &i: t.sideEffects) {
          if(get_default(next.states, i.first, STATE_UNKNOWN) != STATE_UNKNOWN && i.second != get_default(next.states, i.first, STATE_UNKNOWN)) {
            incompatible = true;
            break;
          }

          if(i.second != STATE_UNKNOWN) {
            next.states[i.first] = i.second;
          }
        }
        if(incompatible) continue;

        anyValidTransition = true;

        if(!seen.contains(next)) {
          cout << "=== next state ===" << endl;
          for(size_t i = 0; i < next.actions.size(); ++i) {
            cout << "actions[" << i << "] = " << next.actions[i] << endl;
          }
          for(auto &[space, state]: next.states) {
            cout << "states[" << space << "] = " << state << endl;
          }

          seen.insert(next);
          queue.push_back(next);
        }
      }

      if(!anyValidTransition) incompatible = true;
    }
    if(incompatible) continue;

    bool allKnown = true;
    for(auto &v: current.actions) {
      if(v == STATESPACE_UNKNOWN) allKnown = false;
    }
    for(auto &s: current.states) {
      if(s.second == STATE_UNKNOWN) allKnown = false;
    }
    for(auto &s: statespaces) {
      if(get_default(current.states, s, STATE_UNKNOWN) == STATE_UNKNOWN) allKnown = false;
    }

    if(allKnown) {
      Step step;
      for(auto s: statespaces) {
        for(int i = 0; i < s->portCount(); ++i) {
          step.actions[{s, i}] = current.actions[connections[{s, i}]];
        }

        step.states[s] = current.states[s];
      }

      result.push_back(step);
    }
  }

  return result;
}
