#include "atom.h"

using namespace std;
using namespace semantics;

Statespace::Barb Atom::possibleTransitions(const State *, std::vector<Statespace *>) {
  return {vector<Transition>{Transition{
    .from = this,
    .to = this,
    .actions = { this },
    .sideEffects = {},
  }}};
}
