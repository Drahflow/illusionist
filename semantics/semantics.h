#ifndef H_8E265E65_A357_4BB1_BEAE_07D02A6DBA37
#define H_8E265E65_A357_4BB1_BEAE_07D02A6DBA37

#include <memory>

namespace scene {
  class Multiplanar;
}

namespace semantics {
  class Statespace;

  class Semantics {
    public:
      void run(scene::Multiplanar &);
      std::unique_ptr<Statespace> parse(scene::Multiplanar &);
  };
}

#endif
