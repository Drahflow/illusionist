#include "semantics.h"

#include "statespace.h"

#include "../scene/multiplanar.h"

#include <random>

using namespace std;
using namespace scene;
using namespace semantics;

void Semantics::run(Multiplanar &program) {
  unique_ptr<Statespace> sp = parse(program);

  if(sp->portCount()) {
    program.error() << "Root statespace has non-zero port count: " << sp->portCount() << endl;
    return;
  }

  static std::random_device rd;
  static std::mt19937 random(rd());

  bool running = true;
  while(running) {
    auto barb = sp->possibleTransitions(sp->currentState(), {});

    if(auto uncomputable = get_if<Uncomputable>(&barb); uncomputable) {
      program.error() << "Uncomputable top-level transition set:  " << uncomputable << endl;
      break;
    }

    auto transitions = get<vector<Transition>>(barb);
    cout << "Total transitions available: " << transitions.size() << endl;

    auto t = transitions.begin();
    advance(t, uniform_int_distribution<decltype(transitions)::size_type>(0, transitions.size() - 1)(random));

    sp->execute(*t);
  }
}

unique_ptr<Statespace> Semantics::parse(Multiplanar &program) {
  Plane *top = program.getTopPlane();
  auto semantics = make_unique<SolvingStatespace>();

  map<PlaneContent *, Statespace *> statespaces;
  map<pair<int, int>, pair<Statespace *, int>> portLocations;

  for(auto &elem: top->getContents()) {
    // TODO: Map zones to separate constructoring statespaces
    // TODO: Filter zoned boxes

    if(auto box = dynamic_cast<PlaneBox *>(elem); box) {
      vector<vector<pair<int, int>>> portsOfEdges;
      portsOfEdges.resize(8);

      for(int edge = 0; edge < 8; ++edge) {
        auto immediateBorder = box->getRegion()->immediateBorder(edge);
        auto withinZone = immediateBorder; // TODO: .intersect(parsedZone); // to exclude connectors from outside zone
        for(auto &[u, v]: withinZone.portOrdering()) {
          PlaneConnector *con = plane->findConnector(u, v);
          if(!con) continue;

          portsOfEdges[edge].push_back({u, v});
        }
      }

      BoxSemantics boxSemantics = resolve(top, box, portsOfEdges);
      statespaces[box] = boxSemantics.statespace;
      for(int edge = 0; edge < 8; ++edge) {
        for(int i = 0; i < portsOfEdges[edge].size(); ++i) {
          portLocations[portsOfEdges[edge][i]] = boxSemantics.getPortIndex(edge, i);
        }
      }
    }
  }

  for(auto &[elem, statespace]: statespaces) semantics.addStatespace(statespace);
  while(!portLocations.empty()) {
    auto [[u, v], [statespace, port]] = *portLocations.begin();
    portLocations.erase({u, v});

    for(auto [ui, vi]: traceConnectors(top, u, v)) {
      if(!portLocations.contains({ui, vi})) continue;
      auto &[statespaceI, portI] = portLocations[{ui, vi}];
      semantics.connect(statespace, port, statespaceI, portI);
      portLocations.erase({ui, vi});
    }
  }
}

Statespace *Semantics::resolve(Plane *, PlaneBox *box) {
  auto label = box->getLabel();

  if(label == "true") {
    return TRUE;
  } else if(label == "false") {
    return FALSE;
  }
}
