#include "common.h"

#include "scene/composition.h"
#include "scene/multiplanar.h"
#include "scene/uniform_plane.h"
#include "scene/plane_content.h"
#include "serialize/json.h"

#include "semantics/semantics.h"

#include <SDL2/SDL_image.h>
#include <SDL2/SDL2_rotozoom.h>
#include <sys/time.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <vector>
#include <fstream>

#define FRAMEDELAY 50

uint64_t now() {
  timeval tv;
  if(gettimeofday(&tv, nullptr) < 0) throw "gettimeofday(2) failed";

  return static_cast<uint64_t>(tv.tv_sec) * 1000000 + tv.tv_usec;
}

int main(int argc, char **argv) {
  try {
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_NOPARACHUTE)) throw "SDL init failed";

    SDL_Window *const window = SDL_CreateWindow("Illusionist",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        width, height, 0);
    SDL_Surface *const screen = SDL_GetWindowSurface(window);

    SDL_Surface *sdlsurf = SDL_CreateRGBSurface(
        0, width, height, 32,
        0x00FF0000, /* Rmask */
        0x0000FF00, /* Gmask */
        0x000000FF, /* Bmask */
        0); /* Amask */

    std::unique_ptr<scene::Multiplanar> program;

    const std::string filename = argc == 2? argv[1]: "default.ils";
    if(std::ifstream file(filename); file) {
      serialize::LoadJson(file).io("program", program);
    } else {
      program = std::make_unique<scene::Multiplanar>();
      program->push_back(std::make_unique<scene::UniformPlane>());
    }

    std::vector<scene::Multiplanar *> focusStack;
    focusStack.push_back(program.get());

    uint64_t nextFrameAt = now() + FRAMEDELAY;
    bool running = true;
    while(running) {
      const auto t = now();

      SDL_Event event;
      while (SDL_PollEvent(&event)) {
        switch(event.type) {
          case SDL_QUIT:
            running = false;
            break;

          default:
            if(event.type == SDL_KEYDOWN) {
              if(event.key.keysym.sym == SDLK_s && SDL_GetModState() & KMOD_CTRL) {
                std::ofstream file(filename);
                serialize::SaveJson(file).io("program", program);
                break;
              }

              if(event.key.keysym.sym == SDLK_F5) {
                semantics::Semantics().run(*program);
                break;
              }
            }

            focusStack.back()->handleEvent(event);
            break;
        }
      }

      if(nextFrameAt < t) {
        SDL_LockSurface(sdlsurf);
        cairo_surface_t *cairosurf = cairo_image_surface_create_for_data(
          reinterpret_cast<unsigned char *>(sdlsurf->pixels),
          CAIRO_FORMAT_RGB24,
          sdlsurf->w,
          sdlsurf->h,
          sdlsurf->pitch);

        cairo_t *cr = cairo_create(cairosurf);

        focusStack.back()->draw(cr);

        // cairo_set_line_width(cr, 3.5);
        // cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.5);
        // cairo_move_to(cr, 250.0, 260.0);
        // cairo_line_to(cr, 150.0, 210.0);
        // cairo_stroke(cr);

        // cairo_set_line_width(cr, 2);
        // cairo_set_source_rgb(cr, 0.7, 0.7, 0.9);
        // cairo_move_to(cr, 250.0, 250.0);
        // cairo_line_to(cr, 150.0, 200.0);
        // cairo_stroke(cr);

        // cairo_set_line_width(cr, 2);
        // cairo_set_source_rgb(cr, 0.7, 0.7, 0.9);
        // cairo_move_to(cr, 350.0, 250.0);
        // cairo_line_to(cr, 250.0, 200.0);
        // cairo_stroke(cr);

        cairo_destroy(cr);

        cairo_surface_flush(cairosurf);
        cairo_surface_destroy(cairosurf);
        SDL_UnlockSurface(sdlsurf);
        SDL_BlitSurface(sdlsurf, nullptr, screen, nullptr);
        SDL_UpdateWindowSurface(window);

        nextFrameAt = t + FRAMEDELAY;
      }

      usleep(nextFrameAt - t);
    }

    SDL_DestroyWindow(window);
    SDL_Quit();
  } catch (const std::string &err) {
    std::cerr << "SDL error: " << SDL_GetError() << std::endl;
    std::cerr << "System error: " << strerror(errno) << std::endl;
    std::cerr << "Reported error: " << err << std::endl;
  } catch (const char *&err) {
    std::cerr << "SDL error: " << SDL_GetError() << std::endl;
    std::cerr << "System error: " << strerror(errno) << std::endl;
    std::cerr << "Reported error: " << err << std::endl;
    return 1;
  }
}
